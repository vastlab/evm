#!/usr/bin/python

# make sure that we are able to find the EVM library
# you can remove this if you have installed EVM via pip
import sys
sys.path.insert(0, 'build/lib')

import EVM, numpy, scipy
from matplotlib import pyplot
pyplot.ioff()
from matplotlib.backends.backend_pdf import PdfPages
pdf = PdfPages("example.pdf")

# instantiate some random 2D test data
numpy.random.seed(42)
class1 = numpy.random.normal((0,0),3,(50,2))
class2 = numpy.random.normal((-10,10),3,(50,2))
class3 = numpy.random.normal((10,-10),3,(50,2))


#############################################
# First, we show the usage of the single EVM

print ("Computing single EVM")
# instantiate an EVM class
evm = EVM.EVM(tailsize=10, cover_threshold=0.7, distance_function=scipy.spatial.distance.euclidean)

# train the EVM with class1 as target class, and the other two classes as negatives
evm.train(positives = class1, negatives = numpy.concatenate((class2, class3)))

# first, plot points of the three classes
f = pyplot.figure(figsize=(6,6))
pyplot.plot(class1[:,0], class1[:,1], 'ro')
pyplot.plot(class2[:,0], class2[:,1], 'gd')
pyplot.plot(class3[:,0], class3[:,1], 'bh')

# mark the extreme vectors of the positive class
pyplot.plot(evm.extreme_vectors[:,0], evm.extreme_vectors[:,1], 'kx', mew=2)

# finalize plot
pyplot.title("Extreme Vectors of Single Class")
pyplot.axis("square")
pyplot.xticks([-10, 0, 10])
pyplot.yticks([-10, 0, 10])
pyplot.xlim(-20,20)
pyplot.ylim(-20,20)
pdf.savefig(f)


# Compute the probabilities in a grid
f = pyplot.figure(figsize=(6,6))
probs = numpy.zeros((401,401,3))
for i,y in enumerate(numpy.arange(-20, 20.01, 0.1)):
  for j,x in enumerate(numpy.arange(-20, 20.01, 0.1)):
    prob, ev_index = evm.max_probabilities([[x,y]])
    # we color-code the probability from 0 (black) to 1 (red)
    probs[i,j,0] = prob[0]

# plot probabilities and finalize plot
pyplot.imshow(probs, origin='lower')
pyplot.title("Coverage of Single Class")
pyplot.xticks([100, 200, 300], [-10, 0, 10])
pyplot.yticks([100, 200, 300], [-10, 0, 10])
pdf.savefig(f)


#############################################
# Second, we show the usage of multiple EVMs

print ("Computing multiple EVMs")

# instantiate a MultipleEVM class
mevm = EVM.MultipleEVM(tailsize=10, cover_threshold=0.7, distance_function=scipy.spatial.distance.euclidean)
# train with all three classes
mevm.train([class1, class2, class3])

# plot points of the three classes
f = pyplot.figure(figsize=(6,6))
pyplot.plot(class1[:,0], class1[:,1], 'ro')
pyplot.plot(class2[:,0], class2[:,1], 'gd')
pyplot.plot(class3[:,0], class3[:,1], 'bh')

# and the according extreme vectors
pyplot.plot(mevm.evms[0].extreme_vectors[:,0], mevm.evms[0].extreme_vectors[:,1], 'kx', mew=2)
pyplot.plot(mevm.evms[1].extreme_vectors[:,0], mevm.evms[1].extreme_vectors[:,1], 'kx', mew=2)
pyplot.plot(mevm.evms[2].extreme_vectors[:,0], mevm.evms[2].extreme_vectors[:,1], 'kx', mew=2)

# finalize plot
pyplot.title("Extreme Vectors of Multiple Classes")
pyplot.axis("square")
pyplot.xticks([-10, 0, 10])
pyplot.yticks([-10, 0, 10])
pyplot.xlim(-20,20)
pyplot.ylim(-20,20)
pdf.savefig(f)

# plot the maximum probability in color-coding
# first class: red, second class green, third class blue
f=pyplot.figure(figsize=(6,6))
probs = numpy.zeros((401,401,3))
for i,y in enumerate(numpy.arange(-20, 20.01, 0.1)):
  for j,x in enumerate(numpy.arange(-20, 20.01, 0.1)):
    # compute maximum probability and the index of the EVM class
    prob, evm_index = mevm.max_probabilities([[x,y]])
    # color-code using the evm information
    probs[i,j, evm_index[0][0]] = prob[0]

# finalize the plot
pyplot.imshow(probs, origin='lower')
pyplot.title("Coverage of Multiple Classes")
pyplot.xticks([100, 200, 300], [-10, 0, 10])
pyplot.yticks([100, 200, 300], [-10, 0, 10])
pdf.savefig(f)

print ("Wrote example.pdf")
pdf.close()
